from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pyseis',

    version='0.1.0',

    description='Pyseis',
    long_description=long_description,

    url='https://github.com/efoley/pyseis',

    author='Eric Foley',
    author_email='ericdfoley@gmail.com',

    license='MIT',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
)
