import array
import struct

# TODO EDF do this as a .pyx since it will be much faster in C++

class TraceCompression:
    def __init__(self, bytes : int, packet_num_samples : int, trace_num_samples : int):
        self.bytes = bytes

        self.trace_num_samples = trace_num_samples

        if self.bytes==4:
            self.packet_num_samples = self.trace_num_samples
            self.trace_num_packets = 1
            self.trace_num_bytes = 4*self.trace_num_samples

            #packet_format = '{}f'.format(self.trace_num_samples)
            #self.packet_struct = struct.Struct(packet_format)
            self.trace_samples_array_def = array.array('f')
            #samples.frombytes(bytes)

        else:
            # NOTE that packet # samples and trace # bytes are both multiples of 4
            #self.packet_num_samples = packet_num_samples + packet_num_samples % 4
            if packet_num_samples%4!=0:
                raise RuntimeError("Packet must be a multiple of 4")

            self.packet_num_samples = packet_num_samples
            self.trace_num_packets = (self.trace_num_samples + self.packet_num_samples - 1)//self.packet_num_samples
            self.trace_num_bytes = ((self.trace_num_samples * self.bytes + self.trace_num_packets * 4 + 3) // 4) * 4

            t = 'b' if self.bytes==1 else 'h'
            self.packet_struct = struct.Struct('f{}{}'.format(self.packet_num_samples, t))
            if self.trace_num_samples % self.packet_num_samples == 0:
                self.last_packet_struct = None
            else:
                self.last_packet_struct = struct.Struct('f{}{}'.format(self.trace_num_samples % self.packet_num_samples, t))

            m = (1 << (self.bytes * 8 - 1)) - 2
            self.nan_int_value = -m - 2
            self.neg_inf_int_value = -m - 1
            self.pos_inf_int_value = m + 1

    def _decompress_sample(self, scalar, v):
        if v == self.nan_int_value:
            return float('nan')
        elif v == self.neg_inf_int_value:
            return float('-inf')
        elif v == self.pos_inf_int_value:
            return float('+inf')
        else:
            return scalar * v

    def _decompress_samples(self, scalar : float, vals):
        assert self.bytes!=4

        return [self._decompress_sample(scalar, v) for v in vals]

    def decode(self, trbytes):
        """
        Decompresses the provided buffer containing the byte-encoding of a single trace.
        :param trbytes: byte array for a single trace as written on disk
        :return: decoded float array of samples for the trace
        """

        samples = array.array('f')

        # 4 byte special case
        if self.bytes==4:
            samples.frombytes(trbytes)
            return samples

        # 1 & 2 byte cases
        samples = array.array('f')

        off = 0
        for p in range(self.trace_num_packets):
            if self.last_packet_struct is not None and p == self.trace_num_packets-1:
                struct = self.last_packet_struct
            else:
                struct = self.packet_struct

            tup = struct.unpack_from(trbytes, off)

            scalar = tup[0]
            vals = tup[1:]
            fvals = self._decompress_samples(scalar, vals)
            samples.fromlist(fvals)

            off += self.packet_num_samples * self.bytes + 4

        return samples

