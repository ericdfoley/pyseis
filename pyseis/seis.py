import array
import bisect
import enum
import json
import os
import struct

import typing

from pyseis import util
from pyseis import tracecompression

class HeaderTypeValue:
    def __init__(self, name):
        self.name = name

class HeaderType(enum.Enum):
    float = HeaderTypeValue("float")
    double = HeaderTypeValue("double")
    int8 = HeaderTypeValue("int8")
    int16 = HeaderTypeValue("int16")
    int32 = HeaderTypeValue("int32")
    int64 = HeaderTypeValue("int64")

    @classmethod
    def parse(cls, name):
        return cls[name.lower()]

    @classmethod
    def size(cls, ht):
        return {cls.float: 4, cls.double: 8, cls.int8: 1, cls.int16: 2, cls.int32: 4, cls.int64: 8}[ht]

    @classmethod
    def is_integral(cls, ht):
        return ht!=cls.float and ht!=cls.double

    @classmethod
    def is_float(cls, ht):
        return ht==cls.float or ht==cls.double

    @classmethod
    def struct_format_char(cls, ht):
        format = {cls.float: 'f', cls.double: 'd', cls.int8: 'b', cls.int16: 'h', cls.int32: 'i', cls.int64: 'l'}
        return format[ht]

    @classmethod
    def unpack(cls, ht, bytes):
        # format strings used by Python struct library
        format = cls.struct_format_char(ht)
        return struct.Struct(format).unpack(bytes)[0]


class HeaderDef:
    def __init__(self, header_def: str):
        n, t, b = header_def.split(":")
        self.name = n
        self.type = HeaderType.parse(t)
        self.byte_offset = int(b)
        self.num_bytes = HeaderType.size(self.type)

    def read_value(self, trace_header):
        v = memoryview(trace_header)
        b0 = self.byte_offset
        b1 = b0 + self.num_bytes
        return HeaderType.unpack(self.type, v[b0:b1])

    def _to_def(self):
        return ':'.join([self.name, self.type.name, str(self.byte_offset)])

    def __repr__(self):
        return "HeaderDef('{}')".format(self._to_def())

    def __str__(self):
        return self._to_def()

class HeaderDefs:
    def __init__(self, defs: typing.Sequence[str]):
        self.elems = [HeaderDef(d) for d in defs]
        self.name_to_elem = {e.name: e for e in self.elems}

        self.total_num_bytes = self._compute_total_num_bytes()

        self.struct = struct.Struct(' '.join(HeaderType.struct_format_char(d.type) for d in self.elems))

        assert self.struct.size==self.total_num_bytes

    def _compute_total_num_bytes(self):
        max_elem = max(self.elems, key=lambda d: d.byte_offset)
        foo = HeaderType.size(max_elem.type)
        return max_elem.byte_offset + HeaderType.size(max_elem.type)

    def __getitem__(self, name: str):
        return self.name_to_elem[name]

    def __repr__(self):
        return "HeaderDefs('{}')".format([str(d) for d in self.elems])

    def __str__(self):
        return str([str(d) for d in self.elems])


class SortMap:
    def __init__(self, seis_data, *keys):
        self.seis_data = seis_data
        self.keys = keys

        self.traces = None

    def build(self):
        traces = self._scan(self.keys) # contains tuples of the form (*keys, itr)
        traces.sort()

        self.traces = traces

    def num_traces(self):
        return len(self.traces)

    def lower_bound(self, *values):
        """        
        :param values: values for some or all of the keys 
        :return: the first trace index in the dataset with key values not less than the provided values
        """
        if self.traces is None:
            raise RuntimeError("build() has not been called")

        # TODO EDF may want to build a prefix tree to make this faster
        itr = 0
        for i,v in enumerate(values):
            while self.traces[itr][i] < v:
                itr += 1
        return itr

    def _scan(self, keys):
        values = []

        header_defs = [self.seis_data.header_defs[k] for k in keys]

        record_num_bytes = self.seis_data.header_defs.total_num_bytes
        record = bytearray(record_num_bytes)
        itr = 0
        for hd_path in self.seis_data.extents.header_paths:
            with open(hd_path, 'rb') as hd_file:
                while hd_file.readinto(record) != 0:
                    vs = [hd.read_value(record) for hd in header_defs]
                    vs.append(itr)
                    values.append(vs)
                    itr += 1

        return values


class Extents:
    def __init__(self, path, extent_paths: typing.Sequence[str]):
        self.path = path
        self.unsubbed_paths = extent_paths

        def _ext_matches(p, ext):
            return os.path.splitext(p)[1].startswith('.'+ext)

        project_root = util.find_project_root(self.path)

        self.header_paths = [util.expand_path(p, project_root) for p in extent_paths if _ext_matches(p, 'hd')]
        self.trace_paths = [util.expand_path(p, project_root) for p in extent_paths if _ext_matches(p, 'tr')]
        self.db_paths = [util.expand_path(p, project_root) for p in extent_paths if _ext_matches(p, 'db')]

    def __repr__(self):
        return "Extents('{}')".format(self.paths)


class SeisData:
    def __init__(self, path: str):
        with open(path, 'r') as f:
            self.metadata = json.load(f)

        self.trace_num_samples = int(self.metadata['n1'])
        #self.trace_num_bytes = 4*self.trace_num_samples

        self.trace_compression = tracecompression.TraceCompression(
            int(self.metadata['bytes']), int(self.metadata['packet']), self.trace_num_samples)

        self.trace_num_bytes = self.trace_compression.trace_num_bytes

        self.header_defs = HeaderDefs(self.metadata['headers'])

        self.extents = Extents(path, self.metadata['extents'])

        self.header_num_bytes = self.header_defs.total_num_bytes

        self._num_traces = None # unknown at this point

    def num_traces(self):
        if self._num_traces is None:
            raise RuntimeError("Dataset has not yet been opened, so the number of traces is unknown.")
        return self._num_traces

    # TODO EDF use a reader type object for this and reading methods to deal with open & close extents
    def start_reading(self):
        # build data structure providing offsets into each extent
        nb = self.header_defs.total_num_bytes

        extent_num_traces = []
        for p in self.extents.header_paths:
            extent_num_traces.append(int(os.path.getsize(p)/nb))

        first_itr = [0]
        for n in extent_num_traces:
            first_itr.append(first_itr[-1] + n)

        self.num_traces = first_itr[-1]

        self.first_extent_itr = first_itr # contains (# extents + 1) entries

        self.max_open_extents = 50
        self.open_extents = [] # contains tuples (extent number, hd fd, tr fd, last read tick)

        self.read_tick = 0

    def _open_file(self, iext, tr_or_hd='hd'):
        for ie,hd,tr,_ in self.open_extents:
            if ie==iext:
                return hd if tr_or_hd=='hd' else tr

        hd = open(self.extents.header_paths[iext], 'rb')
        tr = open(self.extents.trace_paths[iext], 'rb')

        t = (iext, hd, tr, self.read_tick)
        self.open_extents.append(t)

        if len(self.open_extents)>self.max_open_extents:
            oldest = min(self.open_extents, key=lambda t: t[3]) # oldest by read tick
            self.open_extents.remove(oldest)

        return hd if tr_or_hd=='hd' else tr

    def stop_reading(self):
        for _,hd_file, tr_file,_ in self.open_extents:
            hd_file.close()
            tr_file.close()

    def _extent_trace(self, itr):
        iext = bisect.bisect_right(self.first_extent_itr, itr)-1
        itr_in_file = itr - self.first_extent_itr[iext]

        return iext, itr_in_file

    def read_headers(self, itr):
        """
        Read trace headers.
        :param itr: trace index at which to read.
        :return: dict of headers
        """
        iext, itr_in_file = self._extent_trace(itr)

        hd_file = self._open_file(iext, 'hd')
        hd_file.seek(itr_in_file*self.header_num_bytes)
        return hd_file.read(self.header_num_bytes)

    def read_headers_dict(self, itr):
        bytes = self.read_headers(itr)

        values = self.header_defs.struct.unpack(bytes)
        return {d.name: v for d,v in zip(self.header_defs.elems, values)}

    def read_trace(self, itr):
        """
        Read trace samples
        :param itr: trace index at which to read.
        :return: array of trace samples
        """
        iext, itr_in_file = self._extent_trace(itr)

        tr_file = self._open_file(iext, 'tr')
        tr_file.seek(itr_in_file*self.trace_num_bytes)

        bytes = tr_file.read(self.trace_num_bytes)
        #samples = array.array('f')
        #samples.frombytes(bytes)

        #return samples

        return self.trace_compression.decode(bytes)


def gen_traces(sort_map: SortMap):
    seis_data = sort_map.seis_data

    seis_data.start_reading()

    for t in sort_map.traces:
        itr = t[-1]
        headers = seis_data.read_headers_dict(itr)
        samples = seis_data.read_trace(itr)

        yield headers, samples

    seis_data.stop_reading()

