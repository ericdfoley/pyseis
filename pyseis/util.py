import os


def find_project_root(path: str):
    """
    Traverses upwards until it finds a project root containing a '.opencps.project' file. 
    :param path: any path within an OpenCPS project
    :return: root directory of the OpenCPS project or None
    """
    dir = os.path.dirname(path)
    while not os.path.ismount(dir):
        if os.path.exists(os.path.join(dir, '.opencps.grid')):
            return dir
        dir = os.path.dirname(dir)
    return None

def expand_path(path: str, project_root):
    return path.replace("${project}", project_root)
