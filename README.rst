Pyseis: Pure Python Reveal seis dataset reading
================================================

This package provides utilities to read Reveal seis dataset files.

----

Reveal is seismic processing software developed by`Shearwater GeoServices <http://www.shearwatergeo.com>`_.