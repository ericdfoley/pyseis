"""
Run 'pytest -s' to get output.
"""

from pyseis.seis import *


def test_example_seis_file_read():
    path = "/d1/opencps_projects/pyseis_testing/datasets/synth_const.seis"
    seis_data = SeisData(path)
    print(seis_data.header_defs)
    print(seis_data.header_defs['CMP'])

    cmp_sort = SortMap(seis_data, "CMP")
    cmp_sort.build()

    seis_data.start_reading()

    print("Dataset contains {} traces".format(seis_data.num_traces))

    cmp = 1500
    itr = cmp_sort.lower_bound(cmp)
    headers = seis_data.read_headers_dict(itr)
    samples = seis_data.read_trace(itr)
    print("Looking at trace near CMP {}".format(cmp))
    print("Headers are {}".format(headers))
    print("Samples are (skipping 200) {}".format(samples[::200]))


    # TODO EDF assert samples equal to CMP%100-50

    seis_data.stop_reading()

def test_2byte_seis_file_read():
    path = "/d1/opencps_projects/pyseis_testing/datasets/synth_const_2b.seis"
    seis_data = SeisData(path)

    n1 = seis_data.trace_num_samples

    seis_data.start_reading()

    hd = seis_data.read_headers(0)
    tr = seis_data.read_trace(0)

    cmp = seis_data.header_defs['CMP'].read_value(hd)
    #cmp = hd['CMP']

    print(cmp)
    print(tr[0])

    expect = cmp%100 - 50
    actual = tr[0]
    assert expect==actual

    seis_data.stop_reading()
